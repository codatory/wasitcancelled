require 'test_helper'

class SeriesTest < ActiveSupport::TestCase
  test "tvdb loads" do
    assert Series.first.tvdb.present?
  end

  test "tvdb updates" do
    s = Series.first
    s.update_from_tvdb_if_stale
    s.reload
    assert s.last_updated < 5.minutes.ago
  end
end
