namespace :import do
  desc 'Download and import all shows'
  task :all => :environment do
    puts 'Beginning update...'
    print 'Deleting all old series...'
    puts Series.delete_all
    require 'open-uri'
    require 'ox'
    puts 'Downloading update file'
    mirror = TVDB.mirror_urls.sample
    raw_xml = open("#{mirror}/api/#{ENV['TVDB_API_KEY']}/updates/updates_all.xml")
    parsed_xml = Ox.parse(raw_xml.read)
    series     = parsed_xml.root.nodes.select{|i| i.value == 'Series'}
    p = ProgressBar.new(series.count, :bar, :rate, :counter, :elapsed, :eta)
    series.each do |node|
      Series.find_or_initialize_by_tvdb_id(node.id.nodes.first).update_from_tvdb!
      p.increment!
    end
    p.finish
  end

  desc 'Download and import daily'
  task :daily => :environment do
    puts 'Beginning update...'
    require 'open-uri'
    require 'ox'
    puts 'Downloading update file'
    mirror = TVDB.mirror_urls.sample
    raw_xml = open("#{mirror}/api/#{ENV['TVDB_API_KEY']}/updates/updates_day.xml")
    parsed_xml = Ox.parse(raw_xml.read)
    series     = parsed_xml.root.nodes.select{|i| i.value == 'Series'}
    p = ProgressBar.new(series.count, :bar, :rate, :counter, :elapsed, :eta)
    series.each do |node|
      Series.find_or_create_by_tvdb_id(node.id.nodes.first).update_from_tvdb!
      p.increment!
    end
    p.finish
  end

  desc 'Download and import weekly'
  task :weekly => :environment do
    puts 'Beginning update...'
    require 'open-uri'
    require 'ox'
    puts 'Downloading update file'
    mirror = TVDB.mirror_urls.sample
    raw_xml = open("#{mirror}/api/#{ENV['TVDB_API_KEY']}/updates/updates_week.xml")
    parsed_xml = Ox.parse(raw_xml.read)
    series     = parsed_xml.root.nodes.select{|i| i.value == 'Series'}
    p = ProgressBar.new(series.count, :bar, :rate, :counter, :elapsed, :eta)
    series.each do |node|
      Series.find_or_create_by_tvdb_id(node.id.nodes.first).update_from_tvdb!
      p.increment!
    end
    p.finish
  end

  desc 'Download and import monthly'
  task :monthly => :environment do
    puts 'Beginning update...'
    require 'open-uri'
    require 'ox'
    puts 'Downloading update file'
    mirror = TVDB.mirror_urls.sample
    raw_xml = open("#{mirror}/api/#{ENV['TVDB_API_KEY']}/updates/updates_month.xml")
    parsed_xml = Ox.parse(raw_xml.read)
    series     = parsed_xml.root.nodes.select{|i| i.value == 'Series'}
    p = ProgressBar.new(series.count, :bar, :rate, :counter, :elapsed, :eta)
    series.each do |node|
      Series.find_or_create_by_tvdb_id(node.id.nodes.first).update_from_tvdb!
      p.increment!
    end
    p.finish
  end
end
