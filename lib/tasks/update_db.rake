namespace :update do
  desc 'Performs updates of data'
  task :shows => :environment do
    Series.where(['update_after < ?', Date.today]).limit(1000).each do |s|
      print "Fetching data for #{s.title}..."
      begin
        r = TvRage.new(s.title)
      rescue
        puts "failed!"
        s.update_attribute(:update_after, 7.days.from_now)
        next
      end
      s.update_attributes(
         :url            => r.url,
         :premiered      => r.premiered,
         :started_on     => r.started_on,
         :ended_on       => r.ended_on,
         :latest_episode => r.latest_episode.to_json,
         :next_episode   => r.next_episode.to_json,
         :country        => r.country,
         :status         => r.status,
         :genres         => r.genres.join(','),
         :network        => r.network,
         :airtime        => r.airtime,
         :runtime        => r.runtime,
         :tvrage         => r.id,
         :update_after   => nil
      )
      puts "success"
    end
  end
end
