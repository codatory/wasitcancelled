class ReplaceDateColumnsWithStrings < ActiveRecord::Migration
  def up
    change_column :series, :started_on, :string
    change_column :series, :ended_on,   :string
  end

  def down
    change_column :series, :started_on, :date
    change_column :series, :ended_on,   :date
  end
end
