class PresetUpdateAfterFlag < ActiveRecord::Migration
  def up
    Series.update_all(:update_after => 2.days.ago)
  end

  def down
    Series.update_all(:update_after => nil)
  end
end
