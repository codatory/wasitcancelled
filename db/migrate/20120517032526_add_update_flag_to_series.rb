class AddUpdateFlagToSeries < ActiveRecord::Migration
  def change
    add_column :series, :update_after, :date
  end
end
