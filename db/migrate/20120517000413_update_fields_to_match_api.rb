class UpdateFieldsToMatchApi < ActiveRecord::Migration
  def up
    add_column :series, :url,            :string
    add_column :series, :premiered,      :string
    add_column :series, :latest_episode, :string
    add_column :series, :next_episode,   :string
    add_column :series, :status,         :string
    add_column :series, :classification, :string
    add_column :series, :genres,         :string
    add_column :series, :airtime,        :string
  end

  def down
    remove_column :series, :url
    remove_column :series, :premiered
    remove_column :series, :latest_episode
    remove_column :series, :next_episode
    remove_column :series, :status
    remove_column :series, :classification
    remove_column :series, :genres
    remove_column :series, :airtime
  end
end
