class CreateSeries < ActiveRecord::Migration
  def change
    create_table :series do |t|
      t.string :title
      t.string :directory
      t.string :tvrage
      t.date :started_on
      t.date :ended_on
      t.string :episode_count
      t.string :runtime
      t.string :network
      t.string :country

      t.timestamps
    end
  end
end
