class AddTvdbId < ActiveRecord::Migration
  def change
    remove_column :series, :directory
    remove_column :series, :url
    remove_column :series, :tvrage
    remove_column :series, :next_episode
    remove_column :series, :airtime
    remove_column :series, :started_on
    remove_column :series, :ended_on
    remove_column :series, :episode_count
    remove_column :series, :runtime
    remove_column :series, :country
    remove_column :series, :classification
    remove_column :series, :latest_episode
    remove_column :series, :premiered
    add_column :series, :tvdb_id, :integer
    add_column :series, :banner_url, :string
    add_column :series, :airs_day_of_week, :string
    add_column :series, :airs_time_of_day, :string
    add_column :series, :runtime, :integer
    add_column :series, :description, :text
    add_column :series, :last_updated, :datetime, :default => Time.at(0)
    add_column :series, :premiered, :datetime
  end
end
