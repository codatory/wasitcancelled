# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120723030354) do

  create_table "series", :force => true do |t|
    t.string   "title"
    t.string   "network"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.string   "status"
    t.string   "genres"
    t.date     "update_after"
    t.integer  "tvdb_id"
    t.string   "banner_url"
    t.string   "airs_day_of_week"
    t.string   "airs_time_of_day"
    t.integer  "runtime"
    t.text     "description"
    t.datetime "last_updated",     :default => '1970-01-01 00:00:00'
    t.datetime "premiered"
  end

end
