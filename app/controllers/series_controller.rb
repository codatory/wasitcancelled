class SeriesController < ApplicationController
  require 'open-uri'
  autocomplete :series, :title, :full => true
  caches_action :show

  def index
    if request.post?
      @series = Series.where(['title LIKE ?', "%#{params[:title]}%"])
    end
  end

  def show
    @series = Series.find(params[:id])
    if @series.nil?
      redirect_to '/'
    else
      @series.update_from_tvdb_if_stale
    end
    fresh_when :last_modified => @series.updated_at.utc, :etag => @series
    expires_in Time.now.end_of_day, :public => true

    respond_to do |format|
      format.html
      format.jpg do
        if @series.banner_url.blank?
          redirect_to view_context.image_path('notfound.jpg')
        else
          response.headers['Content-Type']  = 'image/jpeg'
          render :text => open(@series.banner_url, 'rb').read
        end
      end
    end
  end

end
