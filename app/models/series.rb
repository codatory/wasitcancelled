class Series < ActiveRecord::Base
  attr_accessible :airs_day_of_week, :airs_time_of_day, :banner_url, :classification, :created_at, :description, :genres, :last_updated, :latest_episode, :network, :premiered, :runtime, :status, :title, :tvdb_id, :update_after, :updated_at

  def tvdb
    begin
      @tvdb ||= if tvdb_id.present?
        TVDB.find_series_by_id(tvdb_id)
      else
        TVDB.fetch_series_from_data(:title => title)
      end
    rescue
      @tvdb = nil
    end
  end

  def stale?
    status != "Ended" && last_updated < 3.months.ago
  end

  def update_from_tvdb_if_stale
    update_from_tvdb! if stale?
  end

  def update_from_tvdb!
    update_attributes(
      :airs_day_of_week => tvdb.try(:airs_day_of_week),
      :airs_time_of_day => tvdb.try(:airs_time),
      :banner_url       => tvdb.try(:banner),
      :description      => tvdb.try(:overview),
      :genres           => tvdb.try(:genre).try(:join, ', '),
      :last_updated     => Time.at(tvdb.try(:lastupdated).to_i),
      :network          => tvdb.try(:network),
      :premiered        => tvdb.try(:first_aired),
      :runtime          => tvdb.try(:runtime).to_i,
      :status           => tvdb.try(:status),
      :title            => tvdb.try(:series_name),
      :tvdb_id          => tvdb.try(:series_id)
    ) unless updated_at && updated_at < 4.hours.ago
  end
end
