class TvRage
  require 'open-uri'
  def initialize(title)
    name = URI.encode(title)
    @data = {}
    open("http://services.tvrage.com/tools/quickinfo.php?show=#{name}").each_line do |line|
      line.split("\n").each do |l|
        k,v = l.split('@')
        @data[k.gsub(/<.*>/,'')] = v
      end
    end
  end

  def id
    @data['Show ID']
  end

  def name
    @data['Show Name']
  end

  def url
    @data['Show URL']
  end

  def premiered
    @data['Premiered']
  end

  def started_on
    Date.parse(@data['Started']) if @data['Started'].present?
  end

  def ended_on
    Date.parse(@data['Ended']) if @data['Ended'].present?
  end

  def latest_episode
    parse_episode @data['Latest Episode']
  end

  def next_episode
    parse_episode @data['Next Episode']
  end

  def country
    @data['Country']
  end

  def status
    @data['Status']
  end

  def classification
    @data['Classification']
  end

  def genres
    @data['Genres'].present? ? @data['Genres'].split(' | ') : []
  end

  def network
    @data['Network']
  end

  def airtime
    @data['Airtime']
  end

  def runtime
    @data['Runtime']
  end

  private
  def parse_episode(raw)
    return nil if raw.nil?
    ary = raw.split('^')
    {
      :season  => ary[0].split('x')[0],
      :episode => ary[0].split('x')[1],
      :title   => ary[1],
      :date    => Date.parse(ary[2])
    }
  end
end
