if ENV['TVDB_API_KEY']
  TVDB = Tvdbr::Client.new ENV['TVDB_API_KEY']
else
  TVDB = nil
end
