require "bundler/capistrano"

set :application, "wasitcancelled"
set :repository,  "git@bitbucket.org:codatory/wasitcancelled.git"

set :keep_releases, 3
set :user, 'deploy'
set :deploy_via, :remote_cache
set :deploy_to, '/var/apps/wasitcancelled/'

set :default_environment, {
  'PATH' => "/opt/ruby/bin/:$PATH"
}

role :web, "rails.codatory.net"                   # Your HTTP server, Apache/etc
role :app, "rails.codatory.net"                   # This may be the same as your `Web` server
role :db,  "rails.codatory.net", :primary => true # This is where Rails migrations will run

after "deploy:restart", "deploy:cleanup"

namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end
