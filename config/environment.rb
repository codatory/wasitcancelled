# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Isitcancelled::Application.initialize!

Mime::Type.register 'image/jpg', :jpg
